import Log from "@/models/Log";
import { useCallback, useState } from "react";

class Hooks {
    static useLogging = () => {
        const [ logs, setLog ] = useState<Log[]>([]);
        const appendLog = useCallback((newLog: Log) => {
            setLog((log) => [newLog, ...log]);
        }, []);
    
        const appendErrorHistory = useCallback((logMessage: string) => {
            appendLog({ color: 'danger', type: 'error', message: `[${Date()}] ${logMessage}` });
            window.alert(logMessage);
        }, [appendLog]);
    
        const appendInfoHistory = useCallback((logMessage: string) => {
            appendLog({ color: 'secondary', type: 'info', message: `[${Date()}] ${logMessage}` });
        }, [appendLog]);
        
        return { logs, appendErrorHistory, appendInfoHistory };
    }   
}
export default Hooks;