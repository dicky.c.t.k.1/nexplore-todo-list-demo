import { App } from 'antd';
import PageContainer from '@/components/PageContainer';
import PageHeader from '@/components/PageHeader';
import HomePage from '@/pages/Home';

const ToDoListApp: React.FC = () => {
  return (
    <App>
      <PageContainer>
        <PageHeader title="To-Do List Demo" />
        <HomePage />
      </PageContainer>
    </App>
  );
}

export default ToDoListApp;