import AddForm from "@/components/AddForm";
import LogPanel from "@/components/LogPanel";
import ModifyDutyModal from "@/components/ModifyDutyModal";
import PageSection from "@/components/PageSection";
import TodoListDataTable, { TodoListDataTableHandle } from "@/components/TodoListDataTable";
import Hooks from "@/hooks";
import Section from "@/models/Section";
import Utils from "@/utils";
import { Divider, FormInstance, Modal } from "antd";
import axios from "axios";
import { useCallback, useMemo, useRef, useState } from "react";
import { Fragment } from "react/jsx-runtime";
import Duty from "@todolist/core/models/Duty";

const HomePage: React.FC = () => {
    const dataTableRef = useRef<TodoListDataTableHandle | null>(null);
    const { logs, appendErrorHistory, appendInfoHistory } = Hooks.useLogging();
    const [ createLoading, setCreateLoading ] = useState<boolean>(false);
    const [ modifyingDutyId, setModifyingDutyId ] = useState<string | undefined>();
    const [ confirmModifying, setConfirmModifying ] = useState<boolean>(false);
    const [ removingDutyIds, setRemovingDutyIds ] = useState<string[]>([]);
    const modifyFormRef = useRef<FormInstance>(null);
  
    const getToDoList = useCallback(() => dataTableRef.current?.getTodoList(), []);
  
    const addOneToTodoList = useCallback(async (duty: Duty) => {
      try {
        setCreateLoading(true);
        appendInfoHistory(`Creating new duty "${duty.name}"`);
        await axios.post(`/todos`, duty);
        // ** Why i am doing this **
        // Avoid doing array editing but fetch from server to ensure the data consistency (Buggy when multiple devices are accessing the web)
        // X const newTodoList = [...todoList, duty];
        // V dataTableRef.current?.refetch();
        appendInfoHistory(`Created new duty "${duty.name}"`);
        dataTableRef.current?.refetch();
      } catch (error: unknown) {
        appendErrorHistory(Utils.errorNormalizer(error));
      } finally {
        setCreateLoading(false);
      }
    }, [appendErrorHistory, appendInfoHistory]);
    
    const removeOneFromTodoList = useCallback(async (duty: Duty) => {
      try {
        setRemovingDutyIds([...removingDutyIds, duty.id]);
        appendInfoHistory(`Removing duty "${duty.name}"`);
        await axios.delete(`/todos/${duty.id}`);
        // ** Why i am doing this **
        // Avoid doing array editing but fetch from server to ensure the data consistency
        // X const newTodoList = todoList.filter((item) => item.id !== duty.id);
        // V dataTableRef.current?.refetch();
        appendInfoHistory(`Removed duty "${duty.name}"`);
        dataTableRef.current?.refetch();
      } catch (error: unknown) {
        appendErrorHistory(Utils.errorNormalizer(error));
      } finally {
        setRemovingDutyIds(removingDutyIds.filter((id) => id !== duty.id));
      }
    }, [appendErrorHistory, appendInfoHistory, removingDutyIds]);
    
    const updateOneFromToDoList = useCallback(async (duty: Duty, newName: string) => {
      try {
        setConfirmModifying(true);
        appendInfoHistory(`Modifying duty "${duty.name}" to "${newName}"`);
        await axios.put(`/todos/${duty.id}`, { name: newName });
        // ** Why i am doing this **
        // Avoid doing array editing but fetch from server to ensure the data consistency
        // X const newTodoList = todoList.map((item) => item.id === duty.id ? { ...item, name: newName } : item);
        // V dataTableRef.current?.refetch();
        appendInfoHistory(`Modified duty "${duty.name}" to "${newName}"`);
        dataTableRef.current?.refetch();
      } catch (error: unknown) {
        appendErrorHistory(Utils.errorNormalizer(error));
      } finally {
        setConfirmModifying(false);
      }
    }, [appendErrorHistory, appendInfoHistory]);
    
    const onModifyItem = useCallback(async (duty: Duty) => {
        setModifyingDutyId(duty.id);
    }, []);
    const cancelModificationModal = useCallback(() => {
        modifyFormRef.current?.resetFields();
    }, []);
    const cancelModification = useCallback(() => {
        setModifyingDutyId(undefined);
    }, []);
    const confirmModification = useCallback(async () => {
        try {
            const values = await modifyFormRef.current?.validateFields();
            const duty = getToDoList()?.find(duty => duty.id === modifyingDutyId);
            if (duty === undefined) {
              return;
            }
            updateOneFromToDoList(duty, values.name)
            setModifyingDutyId(undefined);
        } catch {
          // Do nothing
        }
    }, [getToDoList, modifyingDutyId, updateOneFromToDoList]);
    
    const onRemoveItem = useCallback((duty: Duty) => {
        Modal.confirm({
            title: 'Confirm Deletion',
            content: `Are you sure to remove duty "${duty.name}"?`,
            onOk: () => removeOneFromTodoList(duty),
        });
    }, [removeOneFromTodoList]);

    const onAddFormFinished = useCallback((values: Duty) => {
        addOneToTodoList(values);
    }, [addOneToTodoList]);
    const onModifyDutyName = useCallback(() => {
        try {
            modifyFormRef.current?.validateFields();
        } catch {
          // Do nothing
        }
    }, []);
    const Sections: Section[] = useMemo(() => [
        {
          title: "Input Data",
          content: (
            <AddForm
              loading={createLoading}
              onFinished={onAddFormFinished}
            />
          )
        },
        {
          title: "Server Data",
          content: (
            <TodoListDataTable
              ref={dataTableRef}
              removingDutyIds={removingDutyIds}
              onModifyItem={onModifyItem}
              onRemoveItem={onRemoveItem}
              onUpdate={appendInfoHistory}
              onError={appendErrorHistory}
            />
          )
        },
        {
          title: "Log",
          content: <LogPanel dataSource={logs} />
        }
    ], [appendErrorHistory, appendInfoHistory, createLoading, logs, onAddFormFinished, onModifyItem, onRemoveItem, removingDutyIds]);
    
    const SectionsWithDivider = useMemo(() => Sections.map((section, index) =>
        <Fragment key={section.title}>
            <PageSection title={section.title}>
            { section.content }
            </PageSection>
            { index < Sections.length - 1 && <Divider /> }
        </Fragment>
    ), [Sections]);
    return (
        <>
            {SectionsWithDivider}
            <ModifyDutyModal
                getToDoList={getToDoList}
                modifyingDutyId={modifyingDutyId}
                confirmModifying={confirmModifying}
                modifyFormRef={modifyFormRef}
                confirmModification={confirmModification}
                cancelModification={cancelModification}
                cancelModificationModal={cancelModificationModal}
                onModifyDutyName={onModifyDutyName}
            />
        </>
    );
};
export default HomePage;
