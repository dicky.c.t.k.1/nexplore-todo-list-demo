import ToDoListApp from '../App';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom'
import APIPaginationResult from '@todolist/core/models/APIPaginationResult';
import Duty from '@todolist/core/models/Duty';
jest.mock('axios');
import axios from 'axios';

const mockAxios = axios as jest.Mocked<typeof axios>;

describe('TodoList Web', () => {
  beforeAll(() => {
    // Jest official workaround for window.matchMedia / window.alert error
    window.alert = () => {};
    window.matchMedia = (query: string) => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    });
  });
  afterEach(async () => {
    jest.clearAllMocks();
  });
  describe('TodoList Web - Functionality Test', () => {
    test('fetches to-do list and displays it', async () => {
      const mockData: APIPaginationResult<Duty> = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
      render(<ToDoListApp />);
      // Wait for the API call to complete
      await waitFor(() => {
        expect(screen.getByText('Task 1')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 2')).toBeInTheDocument();
      });
    });
    test('click refresh button to refetch', async () => {
      let mockData: APIPaginationResult<Duty> = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
      render(<ToDoListApp />);
      // Wait for the API call to complete
      await waitFor(() => {
        expect(screen.getByText('Task 1')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 2')).toBeInTheDocument();
      });
      mockData = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
          { id: '3', name: 'Task 3' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
  
      fireEvent.click(screen.getByText('Refresh'));
      await waitFor(() => {
        expect(screen.getByText('Task 1')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 2')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 3')).toBeInTheDocument();
      });
    });
    test('create a new duty', async () => {
      let mockData: APIPaginationResult<Duty> = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
      render(<ToDoListApp />);
      // Wait for the API call to complete
      await waitFor(() => {
        expect(screen.getByText('Task 1')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 2')).toBeInTheDocument();
      });
  
      // Enter a new duty name in the input field
      fireEvent.change(screen.getByPlaceholderText('Duty Name'), { target: { value: 'Task 3' } });
  
      mockData = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
          { id: '3', name: 'Task 3' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
      // Click the Add button
      fireEvent.click(screen.getByText('Add'));
  
      // Wait for the API call to complete and update the todo list
      await waitFor(() => {
        expect(screen.getByText('Task 3')).toBeInTheDocument();
      });
    });
  
    test('remove a duty', async () => {
      let mockData: APIPaginationResult<Duty> = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
      render(<ToDoListApp />);
      
      // Wait for the API call to complete and render the todo list
      await waitFor(() => {
        expect(screen.getByText('Task 1')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 2')).toBeInTheDocument();
      });    
      mockAxios.delete.mockResolvedValueOnce({
        data: {},
        status: 200,
        statusText: 'OK',
        headers: {},
        config: { headers: {} },
      });
      // Click the Remove button for the first duty
      fireEvent.click(screen.getAllByText('Remove')[0]);
  
      await waitFor(() => {
        expect(screen.getByText('Confirm Deletion')).toBeInTheDocument();
      });
  
      mockData = {
        items: [
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
  
      fireEvent.click(screen.getByText('OK'));
      // Wait for the API call to complete and update the todo list
      await waitFor(() => {
        expect(screen.queryByText('Task 1')).not.toBeInTheDocument();
      });
    });
  
    test('modify a duty', async () => {
      let mockData: APIPaginationResult<Duty> = {
        items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
      // Mock the axios.put method
      render(<ToDoListApp />);
      await waitFor(() => {
        expect(screen.getByText('Task 1')).toBeInTheDocument();
      });
      await waitFor(() => {
        expect(screen.getByText('Task 2')).toBeInTheDocument();
      });
      mockAxios.put.mockResolvedValueOnce({
        data: {},
        status: 200,
        statusText: 'OK',
        headers: {},
        config: { headers: {} },
      });
      fireEvent.click(screen.getAllByText('Modify')[0]);
      await waitFor(() => {
        expect(screen.getByText('Modifying Duty')).toBeInTheDocument();
      });
      mockData = {
        items: [
          { id: '1', name: 'Task 1 (Updated)' },
          { id: '2', name: 'Task 2' },
        ],
        totalPages: 1,
        pageSize: 10
      };
      mockAxios.get.mockResolvedValueOnce({ data: mockData });
  
      const input = screen.getByPlaceholderText('Duty New Name');
      // Enter a new name in the modifying duty input field
      fireEvent.change(input, { target: { value: 'Task 1 (Updated)' } });
  
      // Click the OK button in the modal
      fireEvent.click(screen.getByText('OK'));
  
      // Wait for the API call to complete and update the todo list
      await waitFor(() => {
        expect(screen.getByText('Task 1 (Updated)')).toBeInTheDocument();
      });
    });
  });
  
  
  describe('Input Unit Test', () => {
    describe('Add Duty Form', () => {
      test('Input valid duty name', async () => {
        const { container } = render(<ToDoListApp />);
        fireEvent.change(screen.getByPlaceholderText('Duty Name'), { target: { value: 'Task 1' } });
        expect(screen.getByPlaceholderText('Duty Name')).toHaveValue('Task 1');
        await waitFor(() => {
          expect(container.querySelector('.ant-form-item-explain-error')).toBeNull();
        });
      });
      test('Input empty duty name', async () => {
        render(<ToDoListApp />);
        // Trigger the onChange event, one dummy input should be input first
        fireEvent.change(screen.getByPlaceholderText('Duty Name'), { target: { value: 'a' } });
        fireEvent.change(screen.getByPlaceholderText('Duty Name'), { target: { value: '' } });
        expect(screen.getByPlaceholderText('Duty Name')).toHaveValue('');
        await waitFor(() => {
          expect(screen.getByText('Please input your duty name!')).toBeInTheDocument();
        });
      });
      test('Input duty name with 255 length', async () => {
        const { container } = render(<ToDoListApp />);
        const inputValue = 'a'.repeat(255);
        fireEvent.change(screen.getByPlaceholderText('Duty Name'), { target: { value: inputValue } });
        expect(screen.getByPlaceholderText('Duty Name')).toHaveValue(inputValue);
        await waitFor(() => {
          expect(container.querySelector('.ant-form-item-explain-error')).toBeNull();
        });
      });
      test('Input duty name with 256 length', async () => {
        render(<ToDoListApp />);
        const inputValue = 'a'.repeat(256);
        fireEvent.change(screen.getByPlaceholderText('Duty Name'), { target: { value: inputValue } });
        expect(screen.getByPlaceholderText('Duty Name')).toHaveValue(inputValue);
        await waitFor(() => {
          expect(screen.getByText('Duty name cannot exceed 255 characters!')).toBeInTheDocument();
        });
      });
    });
    describe('Modify Duty Form', () => {
      test('Input valid duty name', async () => {
        mockAxios.get.mockResolvedValueOnce({ data: { items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ], totalPages: 1, pageSize: 10 } });
        const { container } = render(<ToDoListApp />);
        // Wait for the API call to complete
        await waitFor(() => {
          expect(screen.getByText('Task 1')).toBeInTheDocument();
        });
        await waitFor(() => {
          expect(screen.getByText('Task 2')).toBeInTheDocument();
        });
        fireEvent.click(screen.getAllByText('Modify')[0]);
        fireEvent.change(screen.getByPlaceholderText('Duty New Name'), { target: { value: 'Task 3' } });
        expect(screen.getByPlaceholderText('Duty New Name')).toHaveValue('Task 3');
        await waitFor(() => {
          expect(container.querySelector('.ant-form-item-explain-error')).toBeNull();
        });
      });
      test('Input empty duty name', async () => {
        mockAxios.get.mockResolvedValueOnce({ data: { items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ], totalPages: 1, pageSize: 10 } });
        render(<ToDoListApp />);
        // Wait for the API call to complete
        await waitFor(() => {
          expect(screen.getByText('Task 1')).toBeInTheDocument();
        });
        await waitFor(() => {
          expect(screen.getByText('Task 2')).toBeInTheDocument();
        });
        fireEvent.click(screen.getAllByText('Modify')[0]);
        fireEvent.change(screen.getByPlaceholderText('Duty New Name'), { target: { value: 'a' } });
        fireEvent.change(screen.getByPlaceholderText('Duty New Name'), { target: { value: '' } });
        expect(screen.getByPlaceholderText('Duty New Name')).toHaveValue('');
        await waitFor(() => {
          expect(screen.getByText('Please input your duty name!')).toBeInTheDocument();
        });
      });
      test('Input duty name with 255 length', async () => {
        mockAxios.get.mockResolvedValueOnce({ data: { items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ], totalPages: 1, pageSize: 10 } });
        const { container } = render(<ToDoListApp />);
        // Wait for the API call to complete
        await waitFor(() => {
          expect(screen.getByText('Task 1')).toBeInTheDocument();
        });
        await waitFor(() => {
          expect(screen.getByText('Task 2')).toBeInTheDocument();
        });
        fireEvent.click(screen.getAllByText('Modify')[0]);
        const inputValue = 'a'.repeat(255);
        fireEvent.change(screen.getByPlaceholderText('Duty New Name'), { target: { value: inputValue } });
        expect(screen.getByPlaceholderText('Duty New Name')).toHaveValue(inputValue);
        await waitFor(() => {
          expect(container.querySelector('.ant-form-item-explain-error')).toBeNull();
        });
      });
      test('Input duty name with 256 length', async () => {
        mockAxios.get.mockResolvedValueOnce({ data: { items: [
          { id: '1', name: 'Task 1' },
          { id: '2', name: 'Task 2' },
        ], totalPages: 1, pageSize: 10 } });
        render(<ToDoListApp />);
        // Wait for the API call to complete
        await waitFor(() => {
          expect(screen.getByText('Task 1')).toBeInTheDocument();
        });
        await waitFor(() => {
          expect(screen.getByText('Task 2')).toBeInTheDocument();
        });
        fireEvent.click(screen.getAllByText('Modify')[0]);
        const inputValue = 'a'.repeat(256);
        fireEvent.change(screen.getByPlaceholderText('Duty New Name'), { target: { value: inputValue } });
        expect(screen.getByPlaceholderText('Duty New Name')).toHaveValue(inputValue);
        await waitFor(() => {
          expect(screen.getByText('Duty name cannot exceed 255 characters!')).toBeInTheDocument();
        });
      });
    });
  });  
});
