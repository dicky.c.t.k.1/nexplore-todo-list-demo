import axios, { AxiosError } from 'axios';

class Utils {
  static errorNormalizer(error: unknown): string {
    if (!axios.isAxiosError(error)) {
      return 'Unknown error occurred';
    }
    const axiosError = error as AxiosError;
    const responseData = axiosError.response?.data as { error?: string };
    if (!responseData.error) {
      return axiosError.message;
    }
    return responseData.error;
  }
}
export default Utils;