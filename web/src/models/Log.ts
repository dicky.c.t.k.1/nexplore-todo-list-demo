export default interface Log {
    color: 'danger' | 'secondary';
    type: 'error' | 'info';
    message: string;
}