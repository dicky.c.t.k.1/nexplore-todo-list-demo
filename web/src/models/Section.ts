import { ReactNode } from "react";

export default interface Section {
    title: string;
    content: ReactNode;
}