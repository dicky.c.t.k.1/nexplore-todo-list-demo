import { Typography } from "antd";
import { memo } from "react";

interface Props {
    title: string;
}
const PageHeader: React.FC<Props> = memo((props) => (
    <Typography.Title level={1}>{props.title}</Typography.Title>
));
export default PageHeader;