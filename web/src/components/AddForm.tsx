// AddForm.tsx
import { Form, Input, Button } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { useRef, useCallback, memo } from 'react';
import Duty from '@todolist/core/models/Duty';
interface AddFormProps {
  loading: boolean;
  onFinished: (values: Duty) => void;
}

const AddForm: React.FC<AddFormProps> = memo(({ loading, onFinished }) => {
  const addFormRef = useRef<FormInstance>(null);

  const handleFormSubmit = useCallback((values: Duty) => {
    onFinished(values);
    addFormRef.current?.resetFields();
  }, [onFinished]);

  return (
    <Form ref={addFormRef} onFinish={handleFormSubmit}>
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Please input your duty name!' },
          { max: 255, message: 'Duty name cannot exceed 255 characters!' }
        ]}
      >
        <Input placeholder="Duty Name" />
      </Form.Item>
      <Form.Item>
        <Button loading={loading} type="primary" htmlType="submit">Add</Button>
      </Form.Item>
    </Form>
  );
});

export default AddForm;