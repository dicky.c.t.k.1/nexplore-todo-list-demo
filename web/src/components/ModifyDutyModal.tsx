import { ButtonProps, Form, FormInstance, Input, Modal } from "antd";
import { memo, useMemo } from "react";
import Duty from "@todolist/core/models/Duty";

interface Props {
    modifyingDutyId: string | undefined;
    confirmModifying: boolean;
    getToDoList: () => Duty[] | undefined;
    modifyFormRef: React.RefObject<FormInstance>;
    confirmModification: () => void;
    cancelModification: () => void;
    cancelModificationModal: () => void;
    onModifyDutyName: () => void;
}
const ModifyDutyModal: React.FC<Props> = memo(({
    modifyingDutyId,
    confirmModifying,
    getToDoList,
    modifyFormRef,
    confirmModification,
    cancelModification,
    cancelModificationModal,
    onModifyDutyName,
  }) => {
    const okButtonProps: ButtonProps = useMemo(() => ({
        loading: confirmModifying,
        htmlType: "submit"
    }), [confirmModifying]);

    return (
      <Modal
        title="Modifying Duty"
        open={modifyingDutyId !== undefined}
        okButtonProps={okButtonProps}
        onOk={confirmModification}
        onCancel={cancelModification}
        destroyOnClose
        afterClose={cancelModificationModal}
        confirmLoading
      >
        <Form ref={modifyFormRef}>
          Duty Current Name <Input placeholder="Duty Name" value={getToDoList()?.find(duty => duty.id === modifyingDutyId)?.name} disabled />
          Duty New Name
          <Form.Item
            name="name"
            rules={[
              { required: true, message: 'Please input your duty name!' },
              { max: 255, message: 'Duty name cannot exceed 255 characters!' } // Add maxLength validation rule
            ]}
          >
            <Input placeholder="Duty New Name" onChange={onModifyDutyName} />
          </Form.Item>
        </Form>
      </Modal>
    );
});
export default ModifyDutyModal;