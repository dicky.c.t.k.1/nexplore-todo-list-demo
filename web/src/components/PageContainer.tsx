import { Flex } from "antd";
import { memo } from "react";

const pageContainerStyle: React.CSSProperties = {
    width: '100%',
    margin: "0 auto"
};


interface Props {
    children: React.ReactNode;
}
const PageContainer: React.FC<Props> = memo((props) => {
    return (
        <Flex vertical gap="middle" justify="center" align="center" style={pageContainerStyle}>
            {props.children}
        </Flex>
    );
});
export default PageContainer;