import Log from "@/models/Log";import { List, Typography } from "antd";
import { memo } from "react";

interface Props {
  dataSource: Log[],
}
const listStyle: React.CSSProperties = {
    maxHeight: '300px',
    overflowY: 'auto'
};
const LogPanel: React.FC<Props> = memo((props) => {
    return (
        <List style={listStyle}
          dataSource={props.dataSource}
          renderItem={(item) => (
            <List.Item>
              <Typography.Text type={item.type === 'error' ? 'danger' : 'secondary'}>[{item.type.toUpperCase()}]</Typography.Text> {item.message}
            </List.Item>
          )}
        />
    );
});
export default LogPanel;