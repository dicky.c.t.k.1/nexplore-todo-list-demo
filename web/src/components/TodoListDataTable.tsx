import { forwardRef, memo, useCallback, useEffect, useImperativeHandle, useMemo, useState } from 'react';
import axios from 'axios';
import { Flex, Button, Table, TablePaginationConfig, Typography } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import Utils from '@/utils';
import { ColumnsType, TableProps } from 'antd/es/table';
import { ParagraphProps } from 'antd/es/typography/Paragraph';
import APIPaginationResult from '@todolist/core/models/APIPaginationResult';
import Duty from '@todolist/core/models/Duty';

interface Props {
    removingDutyIds: string[];
    onUpdate: (message: string) => void;
    onError: (message: string) => void;
    onModifyItem: (duty: Duty) => void;
    onRemoveItem: (duty: Duty) => void;
}
export interface TodoListDataTableHandle {
    refetch: () => void;
    getTodoList: () => Duty[] | undefined;
}
const dutyNameColumn: ParagraphProps = {
    style: {
        maxWidth: 200
    },
    ellipsis: {
        rows: 2,
        expandable: true
    }
};
const TodoListDataTable = memo(forwardRef<TodoListDataTableHandle, Props>((props, ref) => {
    const [ refreshLoading, setRefreshLoading ] = useState<boolean>(false);
    const [ todoList, setTodoList ] = useState<Duty[] | undefined>(undefined);
    const [paginationParams, setPaginationParams] = useState<TablePaginationConfig>({
        current: 1,
        pageSize: 10,
        total: 0,
    });
    const fetchToDoList = useCallback(async () => {
      try {
        setRefreshLoading(true);
        props.onUpdate("Fetching to-do list");
        const response = await axios.get<APIPaginationResult<Duty>>(`/todos`, {
          params: {
            page: paginationParams.current,
            pageSize: paginationParams.pageSize
          }
        });
        props.onUpdate("Fetched to-do list");
        setTodoList(response.data.items);
        setPaginationParams({
          ...paginationParams,
          total: response.data.totalPages * response.data.pageSize
        });
      } catch (error: unknown) {
        props.onError(Utils.errorNormalizer(error));
      } finally {
        setRefreshLoading(false);
      }
    }, [paginationParams, props]);

    useEffect(() => {
      fetchToDoList();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paginationParams.current, paginationParams.pageSize]); // Do not add fetchToDoList to deps to avoid incorrect function loop

    useImperativeHandle(ref, () => ({
        refetch: fetchToDoList,
        getTodoList: () => todoList
    }));
    const toDoListColumns: ColumnsType<Duty> = useMemo(() => [
      {
        title: 'Duty Name',
        dataIndex: 'name',
        key: 'name',
        render: (text: string) => (
          <Typography.Paragraph {...dutyNameColumn}>
            {text}
          </Typography.Paragraph>
        ),
      },
      {
        title: 'Action',
        key: 'action',
        render: (_, record: Duty) => (
          <Flex vertical={false} gap="small">
            <Button onClick={() => props.onModifyItem(record)}>Modify</Button>
            <Button loading={props.removingDutyIds.find(id => id === record.id) !== undefined} onClick={() => props.onRemoveItem(record)} danger>Remove</Button>
          </Flex>
        ),
      },
    ], [props]);

    const handleTableChange: TableProps['onChange'] = useCallback((pagination: TablePaginationConfig) => {
        setPaginationParams(pagination);
    
        // `dataSource` is useless since `pageSize` changed
        if (pagination.pageSize !== paginationParams.pageSize) {
          setTodoList(undefined);
        }
    }, [paginationParams.pageSize]);

    return (
        <Flex vertical gap="small">
            <div>
                <Button loading={refreshLoading} onClick={fetchToDoList}>Refresh <ReloadOutlined /></Button>
            </div>
            <Table loading={refreshLoading} onChange={handleTableChange} pagination={paginationParams} dataSource={todoList} columns={toDoListColumns} rowKey="id" />
        </Flex>
    );
  }));

  export default TodoListDataTable;