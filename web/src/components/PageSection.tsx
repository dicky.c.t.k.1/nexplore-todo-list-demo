import { Card, Flex, Typography } from "antd";
import { memo } from "react";

interface Props {
    title: string;
    children: React.ReactNode;
}
const pageSectionStyle: React.CSSProperties = {
    width: '100%',
};
const titleStyle: React.CSSProperties = {
    margin: "0 20px"
}
const cardStyle: React.CSSProperties = {
    width: "100%",
    maxWidth: "960px",
    margin: "0 20px"
};

const PageSection: React.FC<Props> = memo((props) => {
    return (
        <Flex vertical gap="middle" style={pageSectionStyle}>
            <Typography.Title level={4} style={titleStyle}>{props.title}</Typography.Title>
            <Card style={cardStyle}>
                {props.children}
            </Card>
        </Flex>
    );
});
export default PageSection;