export default {
    preset: 'ts-jest',
    testEnvironment: 'jest-environment-jsdom',
    transform: {
        "^.+\\.tsx?$": "babel-jest" 
    },
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
    }
}