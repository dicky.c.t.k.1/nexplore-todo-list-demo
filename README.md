# Table of Content
- [Project Description](#project-description)
- [Live Demo](#live-demo)
- [Service Ports](#service-ports)
- [Setup Commands](#setup-commands)
    - [Option A - Docker Compose (All-In-One services [Frontend Web & Backend API & Postgres DB])](#option-a---docker-compose-all-in-one-services-frontend-web--backend-api--postgres-db)
    - [Option B - Docker (Launch the service one by one)](#option-b---docker-launch-the-service-one-by-one)
    - [Option C - Run on Local PC (Dev)](#option-c---run-on-local-pc-dev)
- [Test Cases (Frontend)](#test-cases-frontend)
- [Test Cases (Backend)](#test-cases-backend)
- [FAQ](#faq)
    - [Option A - How do I stop the server on Docker Compose?](#option-a---how-do-i-stop-the-server-on-docker-compose)
    - [Option B - How do I stop the server on Docker?](#option-b---how-do-i-stop-the-server-on-docker)
    - [Option C - How to I stop the server on Local PC](#option-c---how-to-i-stop-the-server-on-local-pc)
- [Disclaimer](#disclaimer)

# Project Description

This project consists of two main parts: the Frontend and the Backend. The Frontend is built using React, a popular JavaScript library for building user interfaces. The Frontend utilizes various performance optimization techniques such as memo, useMemo, and useCallback to reduce unnecessary re-renders and improve overall performance. The Backend is built using Express, a minimal and flexible Node.js web application framework. Both parts of the project have been dockerized, meaning they can be run and tested directly using Docker or Docker Compose or Local PC.

# Live Demo
![Frontend UI](./screenshots/frontend_ui.png)

This is a live demo of the project. You can access the frontend user interface by visiting the following link:

[https://nexploredemo.dicky.dev](https://nexploredemo.dicky.dev)

# Service Ports

Here are the ports used by each service in the project:

- Frontend (Web): 3000
- Backend (API): 3001
- DB (Postgres): 5432

# Setup Commands
Please use PowerShell / Git Bash (Windows) OR Bash/Sh (Linux) for execute the commands.

Before running the following commands, make sure you navigate to the project root directory where the README.md is located using the `cd` command.

To set up and run the project using Docker or Docker Compose or Local PC, you can use the following commands:

## Option A - Docker Compose (All-In-One services [Frontend Web & Backend API & Postgres DB])

To run this project, you will need to have Docker and Docker Compose installed on your machine. If you don't have these installed, you can download them from the following links:

- Docker: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)
- Docker Compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

```bash
cd path/to
docker-compose up -d
```

## Option B - Docker (Launch the service one by one)

To run this project, you will need to have Docker and Docker Compose installed on your machine. If you don't have these installed, you can download them from the following links:

- Docker: [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)

To build and run the Docker image for the Frontend (Web):

```bash
cd path/to
docker build -t todolist_web -f ./web/Dockerfile .
docker run -p 3000:3000 todolist_web
```

To build and run the Docker image for the Backend (API):
```bash
cd path/to
docker build -t todolist_api -f ./api/Dockerfile .
docker run -p 3001:3001 todolist_api
```

To build and run the Docker image for the DB (Postgres with created table):
```bash
cd path/to
docker build -t todolist_db .
docker run postgres \
    --name todolist_db \
    -e POSTGRES_DB=postgres \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=password \
    -p 5432:5432 \
    -v ./db/create_table.sql:/docker-entrypoint-initdb.d/create_table.sql
```

## Option C - Run on Local PC (Dev)

If you prefer to run the project locally without Docker, you will need to have Node.js and PostgreSQL installed on your machine. Here's how you can do it:

To build and run for the Frontend (Web):

```bash
cd path/to/web
yarn install
yarn dev
```

To build and run for the Backend (API):

```bash
cd path/to/api
yarn install
yarn dev
```

Database Setup

**You should have a postgres server on `127.0.0.1:5432` or edit `path/to/api/.env` variable for your postgres server configuration.**

Please create a database schema and execute below SQL query, you can also find this script in `path/to/db/create_table.sql`)
```
CREATE TABLE todos (
    id UUID PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION update_updated_at_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated_at = NOW();
   RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_timestamp
BEFORE UPDATE ON todos
FOR EACH ROW
EXECUTE PROCEDURE update_updated_at_column();
```

## Test Cases (Frontend)
![Frontend Test Cases](./screenshots/frontend_testcases.png)

For the frontend, I used Jest along with Mock Axios and Testing Library/React. Mock Axios is used to mock the HTTP requests and responses, while Testing Library/React is used to render the components, fire events, and check the results.

This project is configured with GitLab CI, which automatically runs tests on every commit.

To Run the test manually on Frontend (Web)

```bash
cd path/to/web
yarn test
```

| Test Suite | Test Case | Description |
| --- | --- | --- |
| Functionality Test | fetches to-do list and displays it | Tests if the to-do list is fetched and displayed correctly |
|   | click refresh button to refetch | Tests if the refresh button correctly refetches the to-do list |
|   | create a new duty | Tests if a new duty can be created |
|   | remove a duty | Tests if a duty can be removed |
|   | modify a duty | Tests if a duty can be modified |
| Input Unit Test - Add Duty Form | Input valid duty name | Tests if a valid duty name can be inputted |
|   | Input empty duty name | Tests if an empty duty name can be inputted |
|   | Input duty name with 255 length | Tests if a duty name with 255 characters can be inputted |
|   | Input duty name with 256 length | Tests if a duty name with 256 characters can be inputted |
| Input Unit Test - Modify Duty Form | Input valid duty name | Tests if a valid duty name can be inputted |
|   | Input empty duty name | Tests if an empty duty name can be inputted |
|   | Input duty name with 255 length | Tests if a duty name with 255 characters can be inputted |
|   | Input duty name with 256 length | Tests if a duty name with 256 characters can be inputted |

## Test Cases (Backend)
![Backend Test Cases](./screenshots/backend_testcases.png)
For the backend, I used Jest and Supertest. Jest is used as the testing framework to write the test cases and Supertest is used to test HTTP requests and responses.

This project is configured with GitLab CI, which automatically runs tests on every commit.

To Run the test manually on Backend (API)

| Test Suite | Test Case | Description |
|------------|-----------|-------------|
| GET /todos | "page" & "pageSize" param inputted (?page=1&pageSize=10) | Tests if the server correctly handles valid page and pageSize parameters |
|   | "page" param inputted (?page=1) | Tests if the server correctly handles a valid page parameter |
|   | "page" & "pageSize" params missed | Tests if the server correctly handles missing page and pageSize parameters |
|   | "page" & "pageSize" param input empty string (?page=&pageSize=) | Tests if the server correctly handles empty page and pageSize parameters |
|   | "page" param input empty string (?page=&pageSize=10) | Tests if the server correctly handles an empty page parameter |
|   | "pageSize" param input empty string (?page=1&pageSize=) | Tests if the server correctly handles an empty pageSize parameter |
|   | "page" & "pageSize" param invalid format (?page=A&pageSize=A) | Tests if the server correctly handles invalid format for page and pageSize parameters |
|   | "page" param invalid format (?page=A&pageSize=10) | Tests if the server correctly handles an invalid format for the page parameter |
|   | "pageSize" param invalid format (?page=1&pageSize=A) | Tests if the server correctly handles an invalid format for the pageSize parameter |
|   | "page" param invalid value (?page=0&pageSize=10) | Tests if the server correctly handles an invalid value for the page parameter |
|   | "pageSize" param invalid value (?page=1&pageSize=0) | Tests if the server correctly handles an invalid value for the pageSize parameter |
|   | "pageSize" param invalid value (?page=1&pageSize=101) | Tests if the server correctly handles an invalid value for the pageSize parameter |
| POST /todos | "name" param inputted (Having existed record) | Tests if the server correctly handles a name parameter that already exists in the record |
|   | "name" param inputted (Name length larger than 255) | Tests if the server correctly handles a name parameter with a length larger than 255 |
| PUT /todos/:id | "id" param inputted & "name" param inputted (No matched id record in DB) | Tests if the server correctly handles a non-matching id and name parameter |
|   | "id" param inputted & "name" param inputted (Name length larger than 255) | Tests if the server correctly handles a name parameter with a length larger than 255 |
|   | "id" param inputted & "name" param inputted (Has conflicted record) | Tests if the server correctly handles a conflicting id and name parameter |
| DELETE /todos/:id | "id" param inputted (Existing record) | Tests if the server correctly handles an existing id parameter |
|   | "id" param inputted (No existing record) | Tests if the server correctly handles a non-existing id parameter |
| GET /unknown-route | Invalid route checking | Tests if the server correctly handles an unknown route |

```bash
cd path/to/api
yarn test
```

# FAQ

## Option A - How do I stop the server on Docker Compose?
If you started the server using Docker Compose, you can stop it by running the following command in your terminal:

```bash
docker-compose down
```

This command stops and removes the Docker containers that were created by `docker-compose up -d`.

## Option B - How do I stop the server on Docker?
If you started the server using Docker, you can stop it by running the following command in your terminal:
```bash
docker stop <container-id-or-name>
```
You can get the container ID or name by running the `docker ps` command, which lists all running Docker containers. Replace `<container-id-or-name>` with the actual ID or name of the Docker container you want to stop.

## Option C - How to I stop the server on Local PC
If you started the server locally, you can stop it by pressing Ctrl+C in the terminal where the server is running. This sends a signal to the server process to terminate.

# Disclaimer
- All the code are created by Dicky CHAN without forking any online resources.
- API should not allow all origins in a production environment. The current configuration is only for demonstration purposes only.