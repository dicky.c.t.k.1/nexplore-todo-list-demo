export default interface APIPaginationResult<T> {
    items: T[];
    pageSize: number;
    totalPages: number;
}