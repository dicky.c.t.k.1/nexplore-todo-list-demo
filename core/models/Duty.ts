export default interface Duty {
    id: string;
    name: string;
}