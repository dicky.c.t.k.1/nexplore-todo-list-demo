class DatabaseConstants {
    static readonly MAX_DUTY_NAME_LENGTH: number = 255;
    static readonly MAX_PAGE_SIZE: number = 100;
    static readonly DEFAULT_PAGE_SIZE: number = 10;
}
export default DatabaseConstants;