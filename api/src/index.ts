import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import dutyService from '@/services/dutyService';

const app = express();

app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.get('/', (_: Request, res: Response) => {
  res.send('OK!');
});
app.use("/todos", dutyService);

const port = process.env.PORT || 3001;
export const server = app.listen(port, () => {
  console.log(`Server running on port ${port}`);
  console.log(`Server using DB ${process.env.PG_HOST}:${process.env.PG_PORT}`);
});
export default app;