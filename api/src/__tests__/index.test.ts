import request from "supertest";
import app, { server } from '@/index';
import { Pool } from "pg";

describe("TodoList API", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  afterAll(done => {
    server.close(done);
  });
  describe("GET /todos", () => {
    describe("Valid inputs", () => {
      test('"page" & "pageSize" param inputted (?page=1&pageSize=10)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: [{ count: 2 }],
          })
          .mockReturnValueOnce({
            rows: [
              { id: "1", name: "Task 1" },
              { id: "2", name: "Task 2" },
            ],
          });

        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .get('/todos?page=1&pageSize=10')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty("items");
      });
      test('"page" param inputted (?page=1)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: [{ count: 2 }],
          })
          .mockReturnValueOnce({
            rows: [
              { id: "1", name: "Task 1" },
              { id: "2", name: "Task 2" },
            ],
          });
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .get('/todos?page=1')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty("items");
      });
    });
    describe("Invalid inputs", () => {
      test('"page" & "pageSize" params missed', async () => {
        const response = await request(app)
          .get('/todos')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid page");
      });
      test('"page" & "pageSize" param input empty string (?page=&pageSize=)', async () => {
        const response = await request(app)
          .get('/todos?page=&pageSize=')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid page");
      });
      test('"page" param input empty string (?page=&pageSize=10)', async () => {
        const response = await request(app)
          .get('/todos?page=&pageSize=10')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid page");
      });
      test('"pageSize" param input empty string (?page=1&pageSize=)', async () => {
        const response = await request(app)
          .get('/todos?page=1&pageSize=')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid pageSize");
      });
      test('"page" & "pageSize" param invalid format (?page=A&pageSize=A)', async () => {
        const response = await request(app)
          .get('/todos?page=A&pageSize=A')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid page");
      });
      test('"page" param invalid format (?page=A&pageSize=10)', async () => {
        const response = await request(app)
          .get('/todos?page=A&pageSize=10')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid page");
      });
      test('"pageSize" param invalid format (?page=1&pageSize=A)', async () => {
        const response = await request(app)
          .get('/todos?page=1&pageSize=A')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid pageSize");
      });
      test('"pageSize" param invalid format (?page=1&pageSize=A)', async () => {
        const response = await request(app)
          .get('/todos?page=1&pageSize=A')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid pageSize");
      });
      test('"page" param invalid value (?page=0&pageSize=10)', async () => {
        const response = await request(app)
          .get('/todos?page=0&pageSize=10')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid page");
      });
      test('"pageSize" param invalid value (?page=1&pageSize=0)', async () => {
        const response = await request(app)
          .get('/todos?page=1&pageSize=0')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Invalid pageSize");
      });
      test('"pageSize" param invalid value (?page=1&pageSize=101)', async () => {
        const response = await request(app)
          .get('/todos?page=1&pageSize=101')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "pageSize exceeds maximum limit of 100");
      });
    });
  });
  describe("POST /todos", () => {
    describe("Valid inputs", () => {
      test('"name" param inputted (No existed record)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: []
          })
          .mockReturnValueOnce({
            rowCount: 1
          });
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .post('/todos')
          .send({ name: "Task 3" })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(201);
      });
    });
    describe("Invalid inputs", () => {
      test('"name" param inputted (Having existed record)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: [
              { id: "1", name: "Task 1" },
            ]
          })
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .post('/todos')
          .send({ name: "Task 1" })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Duty name already exists");
      });
      test('"name" param inputted (Name length larger than 255)', async () => {
        const response = await request(app)
          .post('/todos')
          .send({ name: 'a'.repeat(256) })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Duty name is too long");
      });
    });
  });
  describe("PUT /todos/:id", () => {
    describe("Valid inputs", () => {
      test('"id" param inputted & "name" param inputted (No conflicted record)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: [
              { id: "1", name: "Task 1" },
            ]
          })
          .mockReturnValueOnce({
            rows: []
          })
          .mockReturnValueOnce({
            rowCount: 1
          });
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .put('/todos/1')
          .send({ name: "Task 1 (Updated)" })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(200);
      });
    });
    describe("Invalid inputs", () => {
      test('"id" param inputted & "name" param inputted (No matched id record in DB)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: []
          })
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .put('/todos/1')
          .send({ name: "Task 1 (Updated)" })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Duty not found");
      });
      test('"id" param inputted & "name" param inputted (Name length larger than 255)', async () => {
        const response = await request(app)
          .put('/todos/1')
          .send({ name: 'a'.repeat(256) })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Duty name is too long");
      });
      test('"id" param inputted & "name" param inputted (Has conflicted record)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: [
              { id: "1", name: "Task 1" },
            ]
          })
          .mockReturnValueOnce({
            rows: [
              { id: "2", name: "Task 2" }
            ]
          })
          .mockReturnValueOnce({
            rowCount: 1
          });
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .put('/todos/1')
          .send({ name: "Task 2" })
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "New Duty name is already existed");
      });
    });
  });
  describe("DELETE /todos/:id", () => {
    describe("Valid inputs", () => {
      test('"id" param inputted (Existing record)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: [
              { id: "1", name: "Task 1" },
            ],
          })
          .mockReturnValueOnce({
            rowCount: 1
          });
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .delete('/todos/1')
          .set('Accept', 'application/json');
        expect(response.status).toBe(204);
      });
    });
    describe("Invalid inputs", () => {
      test('"id" param inputted (No existing record)', async () => {
        const mockQuery = jest.fn();
        mockQuery
          .mockReturnValueOnce({
            rows: []
          });
        jest.spyOn(Pool.prototype, 'query').mockImplementation(mockQuery);
        const response = await request(app)
          .delete('/todos/1')
          .set('Accept', 'application/json');
        expect(response.header["content-type"]).toMatch("application/json");
        expect(response.status).toBe(400);
        expect(response.body).toHaveProperty("error", "Duty not found");
      });
    });
  });
  describe("GET /unknown-route", () => {
    test("Invalid route checking", async () => {
      const response = await request(app)
        .get('/unknown-route')
        .set('Accept', 'application/json');
      expect(response.status).toBe(404);
    });
  })
});
