import { Request, Response, Router } from "express";
import { v4 as uuidv4 } from 'uuid';
import Postgres from '@/utils/Postgres';
import DatabaseConstants from "@todolist/core/constants/Database";
import Duty from "@todolist/core/models/Duty";
import APIPaginationResult from "@todolist/core/models/APIPaginationResult";

const router = Router();

router.get('/', async (req: Request, res: Response) => {
    const { page, pageSize = DatabaseConstants.DEFAULT_PAGE_SIZE } = req.query;
    const pageInt = Number(page);
    const pageSizeInt = Number(pageSize);

    if (isNaN(pageInt)) {
        return res.status(400).json({ error: 'Invalid page' });
    }
    if (isNaN(pageSizeInt)) {
        return res.status(400).json({ error: 'Invalid pageSize' });
    }
    if (pageInt < 1) {
        return res.status(400).json({ error: 'Invalid page' });
    }
    if (pageSizeInt < 1) {
        return res.status(400).json({ error: 'Invalid pageSize' });
    }``
    if (pageSizeInt > DatabaseConstants.MAX_PAGE_SIZE) {
        return res.status(400).json({ error: `pageSize exceeds maximum limit of ${DatabaseConstants.MAX_PAGE_SIZE}` });
    }

    const offset = (pageInt - 1) * pageSizeInt;

    const totalCountResult = await Postgres.pool.query('SELECT COUNT(*) FROM todos');
    const totalCount = Number(totalCountResult.rows[0].count);

    const queryResult = await Postgres.pool.query('SELECT * FROM todos ORDER BY updated_at DESC OFFSET $1 LIMIT $2', [offset, pageSizeInt]);
    const duties: Duty[] = queryResult.rows.map(row => ({ id: row.id, name: row.name }));
    const totalPages = Math.ceil(totalCount / pageSizeInt);
    const result: APIPaginationResult<Duty> = {
        items: duties,
        pageSize: pageSizeInt,
        totalPages
    }
    res.json(result);
});

router.post('/', async (req: Request, res: Response) => {
    const id = uuidv4();
    const { name } = req.body;

    if (name.length > DatabaseConstants.MAX_DUTY_NAME_LENGTH) {
        return res.status(400).json({ error: 'Duty name is too long' });
    }
    const existingTodo = await Postgres.pool.query('SELECT * FROM todos WHERE name = $1', [name]);
    if (existingTodo.rows.length > 0) {
        return res.status(400).json({ error: 'Duty name already exists' });
    }
    await Postgres.pool.query('INSERT INTO todos (id, name) VALUES ($1, $2)', [id, name]);
    const result: Duty = { id, name };
    res.status(201).json(result);
});

router.put('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    const { name } = req.body;

    if (name.length > DatabaseConstants.MAX_DUTY_NAME_LENGTH) {
        return res.status(400).json({ error: 'Duty name is too long' });
    }

    const existingTodo = await Postgres.pool.query('SELECT * FROM todos WHERE id = $1', [id]);
    if (existingTodo.rows.length === 0) {
        return res.status(400).json({ error: 'Duty not found' });
    }
    const existingDutyForNewName = await Postgres.pool.query('SELECT * FROM todos WHERE name = $1', [name]);
    if (existingDutyForNewName.rows.length > 0) {
        return res.status(400).json({ error: 'New Duty name is already existed' });
    }

    await Postgres.pool.query('UPDATE todos SET name = $1 WHERE id = $2', [name, id]);
    const result: Duty = { id, name };
    res.json(result);
});

router.delete('/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    const existingTodo = await Postgres.pool.query('SELECT * FROM todos WHERE id = $1', [id]);
    if (existingTodo.rows.length === 0) {
        return res.status(400).json({ error: 'Duty not found' });
    }
    await Postgres.pool.query('DELETE FROM todos WHERE id = $1', [id]);
    res.status(204).send();
});

export default router;