export default {
    rootDir: ".",
    preset: "ts-jest",
    testEnvironment: "node",
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1'
    },
    transformIgnorePatterns: [
        "node_modules/(?!(@todolist/core)/)",
    ],
    testMatch: [
        '<rootDir>/src/**/*.spec.ts',
        '<rootDir>/src/**/*.test.ts'
    ]
}